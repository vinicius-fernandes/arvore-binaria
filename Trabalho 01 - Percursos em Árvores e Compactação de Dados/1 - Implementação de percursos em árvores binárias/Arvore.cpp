/* 
 * File:   Arvore.cpp
 * 
 * Created on 23 de Abril de 2018, 17:48
 */

#include <deque>
#include <stddef.h>
#include <iostream>
#include <cstdlib>

#include "Arvore.h"

using namespace std;

Arvore::Arvore() {
    raiz = NULL;
}

Arvore::Arvore(int x) {
    raiz = new No(x);
}

Arvore::Arvore(const Arvore& orig) {
}

Arvore::~Arvore() {
    delete raiz;
}

void Arvore::SetRaiz(No* raiz) {
    this->raiz = raiz;
}

No* Arvore::GetRaiz() const {
    return raiz;
}

bool Arvore::Vazio() {
    return (raiz == NULL);
}

int Arvore::Busca(int x, No*& ptRaiz) {
    int f; //Indica o resultado da busca: 0, 1, 2 ou 3

    if (ptRaiz == NULL)
        f = 0; // Árvore vazia
    else if (x == ptRaiz->GetChave())
        f = 1; //Achou!
    else if (x < ptRaiz->GetChave()) { // Procurar do lado esquerdo

        if (ptRaiz->GetEsq() == NULL)
            f = 2;
        else {
            ptRaiz = ptRaiz->GetEsq(); // Atualizando ponteiro: nova raiz!
            f = Busca(x, ptRaiz);
        }

    } else { // Procurar do lado direito!

        if (ptRaiz->GetDir() == NULL)
            f = 3;
        else {
            ptRaiz = ptRaiz->GetDir(); // Atualizando ponteiro: nova raiz!
            f = Busca(x, ptRaiz);
        }
    }
    return f; // Resultado da busca
}

bool Arvore::Insere(int x, No* ptRaiz) {
    int f;
    No* pt;
    bool insOK = true;
    No* ptAux = ptRaiz; // Salvar raiz, ptAux vai ser modificado por Busca()

    f = Busca(x, ptAux); // ptAux aponta p/ raiz da árvore

    if (f == 1) // Já está na árvore
        insOK = false;
    else { // Decidir de insere na subárvore: essq ou dir
        pt = new No(x); // Alocar novo nó da árvore

        if (f == 0)
            this->raiz = pt; // Nova raiz;
        else if (f == 2)
            ptAux->SetEsq(pt);
        else // f = 3
            ptAux->SetDir(pt);
    }
    return true;
}

int Arvore::LocalizaMin(No*& pt) {
    if(pt == NULL){
        return 1;
    }
    if(pt->GetEsq() != NULL){
        No* esq = pt->GetEsq();
        return LocalizaMin(esq);
    }
    return pt->GetChave();
}

No* Arvore::Remove(int x, No*& ptRaiz) {

    if(ptRaiz == NULL){
        return NULL;
    }
    
    if(x < ptRaiz->GetChave()){
        No* esq = ptRaiz->GetEsq();
        ptRaiz->SetEsq(Remove(x,esq));
    } else if (x > ptRaiz->GetChave()){
        No* dir = ptRaiz->GetDir();
        ptRaiz->SetDir(Remove(x,dir));
    } else {
        
        if(ptRaiz->GetEsq() == NULL && ptRaiz->GetDir() == NULL){
            delete(ptRaiz);
            ptRaiz = NULL;
        } else if (ptRaiz->GetEsq() == NULL){
            No* temp = ptRaiz;
            ptRaiz = ptRaiz->GetDir();
            delete temp;
        } else if (ptRaiz->GetDir() == NULL){
            No* temp = ptRaiz;
            ptRaiz = ptRaiz->GetEsq();
            delete temp;
        } else {
            No* dir = ptRaiz->GetDir();
            int temp = LocalizaMin(dir);
            ptRaiz->SetChave(temp);
            ptRaiz->SetDir(Remove(temp,dir));
        }
    }
    return ptRaiz;
}

void Arvore::PreOrdem(No* raiz) {
    if (!Vazio()) {
        Visita(raiz);

        if (raiz->GetEsq() != NULL)
            PreOrdem(raiz->GetEsq());

        if (raiz->GetDir() != NULL)
            PreOrdem(raiz->GetDir());
    }
}

void Arvore::EmOrdem(No* raiz) {
    if (!Vazio()) {
        if (raiz->GetEsq() != NULL)
            EmOrdem(raiz->GetEsq());

        Visita(raiz);

        if (raiz->GetDir() != NULL)
            EmOrdem(raiz->GetDir());
    }
}

void Arvore::PosOrdem(No* raiz) {
    if (!Vazio()) {
        if (raiz->GetEsq() != NULL)
            PosOrdem(raiz->GetEsq());

        if (raiz->GetDir() != NULL)
            PosOrdem(raiz->GetDir());

        Visita(raiz);
    }
}

void Arvore::EmNivel(No* raiz) {
    if (!Vazio()) {
        deque<No*> fila;

        fila.push_back(raiz);
        while (!fila.empty()) {

            No* no_atual = fila.front();
            fila.pop_front();
            Visita(no_atual);

            if (no_atual->GetEsq() != NULL)
                fila.push_back(no_atual->GetEsq());

            if (no_atual->GetDir() != NULL)
                fila.push_back(no_atual->GetDir());
        }
    }
}

void Arvore::Visita(No* raiz) {
    cout << raiz->GetChave() << ", ";
}

