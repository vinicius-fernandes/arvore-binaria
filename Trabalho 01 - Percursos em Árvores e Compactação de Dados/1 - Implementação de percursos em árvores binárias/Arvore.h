/* 
 * File:   Arvore.h
 *
 * Created on 23 de Abril de 2018, 17:48
 */

#ifndef ARVORE_H
#define ARVORE_H

#include "No.h"

class Arvore {
public:
    Arvore();
    Arvore(int chave);
    Arvore(const Arvore& orig);
    virtual ~Arvore();
    void SetRaiz(No* raiz);
    No* GetRaiz() const;
    bool Vazio();
    int Busca(int x, No*& ptRaiz);
    bool Insere(int chave, No* raiz);
    No* Remove(int x, No*& pt);
    int LocalizaMin(No*& raiz);
    void PreOrdem(No* raiz);
    void EmOrdem(No* raiz);
    void PosOrdem(No* raiz);
    void EmNivel(No* raiz);
    void Visita(No* raiz);
private:
    No* raiz;
};

#endif /* ARVORE_H */

