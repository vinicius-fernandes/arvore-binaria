/* 
 * File:   Huffman.cpp
 * 
 * Created on 22 de Abril de 2018, 22:14
 */

#include <vector>
#include <algorithm>

#include "Huffman.h"

map<char, string> tabela_codigos;

Huffman::Huffman() {
    raiz = NULL;
}

Huffman::Huffman(const Huffman& orig) {
}

Huffman::~Huffman() {
    delete raiz;
}

bool comp(const pair<char, No*> &p1, const pair<char, No*> &p2) {
    return p1.second->GetFreq() < p2.second->GetFreq();
}

bool comp2(No* a, No* b) {
    return a->GetFreq() < b->GetFreq();
}

list<No*> Huffman::ContFrequencias(string texto) {
    map<char, No*> tabela_freq;
    map<char, No*>::iterator it = tabela_freq.begin();
    list<No*> lista;

    for (int i = 0; i < texto.length(); i++) {
        char ch = texto[i];

        if (tabela_freq.count(ch) <= 0)
            tabela_freq.insert(pair<char, No*>(ch, new No(ch)));

        it = tabela_freq.find(ch);
        if (it != tabela_freq.end())
            it->second->SetFreq(it->second->GetFreq() + 1);
    }
    vector<pair<char, No*> > mapVetor;
    copy(tabela_freq.begin(), tabela_freq.end(), back_inserter(mapVetor));
    sort(mapVetor.begin(), mapVetor.end(), comp);

    for (int i = 0; i < mapVetor.size(); i++)
        lista.push_back(new No(mapVetor[i].first, mapVetor[i].second->GetFreq()));

    return lista;
}

No* Huffman::CriarArvore(list<No*> lista) {

    while (true) {
        No* no1 = lista.front();
        //cout << "No1 = " << "(" << no1->GetCh() << " - " << no1->GetFreq() << ")";
        lista.pop_front();

        No* no2 = lista.front();
        //cout << " | No2 = " << "(" << no2->GetCh() << " - " << no2->GetFreq() << ")";
        lista.pop_front();

        No* pai = new No(no1, no2);
        //cout << " | Pai = " << "(" << pai->GetCh() << " - " << pai->GetFreq() << ")" << endl;

        //cout << "esq = " << "(" << pai->GetEsq()->GetCh()
        //        << " - " << pai->GetEsq()->GetFreq() << ")";
        //cout << " | dir = " << "(" << pai->GetDir()->GetCh()
        //        << " - " << pai->GetDir()->GetFreq() << ")" << endl;
        //cout << "\n\n";

        if (lista.empty())
            return pai;

        lista.push_back(pai);
        lista.sort(comp2);
    }
}

map<char, string> Huffman::Percurso(No* raiz, string codigo) {

    if (raiz->GetEsq() == NULL && raiz->GetDir() == NULL)
        tabela_codigos.insert(pair<char, string>(raiz->GetCh(), codigo));
    else {
        Percurso(raiz->GetEsq(), codigo + "0");
        Percurso(raiz->GetDir(), codigo + "1");
    }
    return tabela_codigos;
}

string Huffman::Codificar(string texto) {
    string saida = "";
    raiz = CriarArvore(ContFrequencias(texto));
    map<char, string> tabela_codigo_caracter = Percurso(raiz, "");
    map<char, string>::iterator it = tabela_codigo_caracter.begin();

    for (int i = 0; i < texto.size(); i++) {
        char ch = texto[i];

        it = tabela_codigo_caracter.find(ch);
        if (it != tabela_codigo_caracter.end())
            saida.append(it->second);
    }
    return saida;
}

void Huffman::SetRaiz(No* raiz) {
    this->raiz = raiz;
}

No* Huffman::GetRaiz() const {
    return raiz;
}

