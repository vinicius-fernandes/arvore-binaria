/* 
 * File:   Graphics.h
 *
 * Created on 1 de Maio de 2018, 20:21
 */

#ifndef GRAPHICS_H
#define GRAPHICS_H

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"

#include <unistd.h>
#include <cstdlib>
#include <iostream>
#include <string>
#include <list>
#include <map>
#include <iterator>

#include "No.h"

void ContentInstrucoes() {
    system("clear");
    cout << ANSI_COLOR_RESET;
    cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
    cout << "▐                          CODIFICAÇÃO DE HUFFMAN                           ▐" << endl;
    cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
    cout << "▐ ◉ INSTRUÇÕES                                                              ▐" << endl;
    cout << "▐                                                                           ▐\n"
            "▐ ➼ Para executar o programa informe o nome do arquivo.txt                  ▐\n"
            "▐ de ENTRADA que deseja comprimir logo á frente do nome do programa.        ▐\n"
            "▐                                                                           ▐\n"
            "▐ ➼ Também deve-se informar o nome do arquivo de SAÍDA à frente do arquivo  ▐\n"
            "▐ de entrada (Este arquivo é onde será salvo o texto codificado).           ▐\n"
            "▐                                                                           ▐\n"
            "▐ POR EXEMPLO: ./codifica__o_de_huffman input.txt output.txt                ▐\n";
    cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl << endl;
}

void ContentErro(string erro) {
    cout << ANSI_COLOR_RESET;
    cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
    cout << "▐                        FALHA DURANTE O PROCESSO                           ▐" << endl;
    cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
    cout << ANSI_COLOR_RED << "\n ◉ " << ANSI_COLOR_RESET << erro;
    cout << "\n\n\n";
}

void ContentSucesso(string entrada, string saida, map<char, string> tabela_codigo_caracter) {
    system("clear");
    cout << ANSI_COLOR_RESET;

    int tamanho_normal = entrada.size() * 8;
    int tamanho_comprimido = saida.length();
    double taxa = 100.0 - (tamanho_comprimido * 100.0 / tamanho_normal);

    cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
    cout << "▐                        ARQUIVO DE ENTRADA                                 ▐" << endl;
    cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
    cout << "\n " << entrada << "\n\n\n";

    cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
    cout << "▐                        ARQUIVO DE SAÍDA                                   ▐" << endl;
    cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
    cout << "\n " << saida << "\n\n\n";

    cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
    cout << "▐                        TABELA DE CÓDIGO/CARACTER                          ▐" << endl;
    cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬\n" << endl;
    cout << ANSI_COLOR_BLUE << "  --------------------------------" << endl;
    cout << "  CARACTER   |   CÓDIGO" << endl;
    cout << "  --------------------------------" << ANSI_COLOR_RESET << endl;
    for (map<char, string>::iterator it = tabela_codigo_caracter.begin(); it != tabela_codigo_caracter.end(); it++) {
        cout << "  " << it->first << "\t     |   " << it->second << endl;
        cout << "  " << "--------------------------------" << endl;
    }
    cout << "\n\n";

    cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
    cout << "▐                        ARQUIVO COMPRIMIDO COM SUCESSO                     ▐" << endl;
    cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
    cout << ANSI_COLOR_GREEN << "  ◉" << ANSI_COLOR_RESET << " O arquivo de ENTRADA foi comprimido e armazenado no arquivo de SAÍDA    \n"
            "    com sucesso.                                                             \n"
            "                                                                             \n"
            "  ➼ TAMANHO NORMAL: " << tamanho_normal << " bits" << "                      \n"
            "  ➼ TAMANHO COMPRIMIDO: " << tamanho_comprimido << " bits" << "              \n"
            "  ➼ O ARQUIVO COMPRIMIDO É " << ANSI_COLOR_GREEN << taxa << ANSI_COLOR_RESET
            << "% MENOR QUE O ORIGINAL.             \n\n\n\n";
}

void BarraComprimindo() {
    system("clear");
    cout << ANSI_COLOR_RESET;

    int i, j, c;
    string s[4] = {"◔", "◑", "◕", "◉"};
    cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
    cout << "▐                        COMPRIMINDO ARQUIVO ...                            ▐" << endl;
    cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬\n" << endl;

    for (i = 0; i <= 70; i++) {
        printf("  %d%%\r", i);
        fflush(stdout);

        for (j = 0; j < i; j++) {
            if (!j) // Da espaco na barra pra nao enconstar na borda da tela
                cout << "  " << s[c] << " ";
            if (c <= 3)
                c++;
            else
                c = 0;
            cout << ANSI_COLOR_GREEN;
            cout << "▒";
            usleep(4000);
        }
    }
    usleep(500);
    system("clear");
    cout << "\n\n\n";
}

#endif /* GRAPHICS_H */

